﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SupportExtendtion{

    public static AttributeEntity CreateAttribute(int health)
    {
        var attributeContext = Contexts.sharedInstance.attribute;
        var newAtttribute = attributeContext.CreateEntity();
        newAtttribute.AddHealth(health);

        return newAtttribute;
    }

    public static GameEntity CreateHero(this GameContext context, Vector2 position, string name)
    {
        var newhero = context.CreateEntity();
        newhero.AddAsset(name);
        newhero.AddPosition(position);
        newhero.AddAnimatePosition(position, context.settingGlobal.value.heroSpeed);
        newhero.AddRangeDetect(3);
        newhero.AddMoveable(false, true);
        newhero.AddAttackTimer(0, 1, false);

        newhero.AddAttribute(CreateAttribute(100));
        //var weapon = CreateWeapon(context);

        return newhero;
    }
        
    public static GameEntity CreateEnemy(this GameContext context, Vector2 position)
    {
        var newhero = context.CreateEntity();
        newhero.AddAsset(RES.Enemy);
        newhero.AddPosition(position);
        newhero.AddAttribute(CreateAttribute(100));
        //    newhero.AddAnimatePosition(new Vector2(2, 0));
        return newhero;
    }

    public static GameEntity CreateFireBall(this GameContext context, Vector2 position)
    {
        var fireBall = context.CreateEntity();
        fireBall.AddAsset(RES.FireBall);
        fireBall.AddPosition(position);

        return fireBall;
    }

    private static GameEntity CreateWeapon(GameContext context)
    {
        var newWeapon = context.CreateEntity();
        newWeapon.AddAsset(RES.Weapon);
        return newWeapon;
    }

    public static GameEntity GetGameEntityFromObject(this GameObject go)
    {
        var gameContext = Contexts.sharedInstance.game;
        var listView = gameContext.GetGroup(GameMatcher.View).GetEntities();

        foreach (var entity in listView)
        {
            if (entity.view.gameObject == go)
                return entity;
        }

        return null;
    }
}

public static class RES
{
    public static readonly string Hero1 = "Player";
    public static readonly string Hero2 = "Player 2";
    public static readonly string Hero3 = "Player 3";
    public static readonly string Enemy = "Enemy";
    public static readonly string FireBall = "FireBall";
    public static readonly string Weapon = "Weapon";
}