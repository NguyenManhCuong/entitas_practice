﻿using UnityEngine;
using Entitas;
using System;

public class AttackSystem : IExecuteSystem{

    private GameContext _context;

    public AttackSystem(Contexts context)
    {
        _context = context.game;
    }

    public void Execute()
    {
        var entities = _context.GetGroup(GameMatcher.AllOf(GameMatcher.Attackable, GameMatcher.AttackTimer)).GetEntities();

        foreach (var entity in entities)
        {
            var target = entity.attackable.target;

            if (CheckDelay(entity))
            {
                Shoot(target.position.value, entity.animatePosition.value);
            }
            
        }
    }

    private bool CheckDelay(GameEntity entity)
    {
        if (!entity.hasAttackTimer)
            return false;

        if (entity.attackTimer.value < entity.attackTimer.targetTime)
        {
            return false;
        }
        else
        {
            entity.attackTimer.value = 0;
            return true;
        }
            
    }

    private void Shoot(Vector2 targetPos, Vector2 spawnPos)
    {
        var fireBall = _context.CreateFireBall(spawnPos);

        var direction = targetPos - spawnPos;

        fireBall.AddFly(direction, 5);
    }
}
