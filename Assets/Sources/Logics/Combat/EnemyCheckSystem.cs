﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using System;

public class EnemyCheckSystem : IExecuteSystem {

    private GameContext gameContext;

    public EnemyCheckSystem(Contexts context)
    {
        gameContext = context.game;
    }

    public void Execute()
    {
        var checkableObject = gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.RangeDetect)).GetEntities();

        foreach (var _object in checkableObject)
        {
            var foundEnemy = CheckEnemy(_object.position.value, _object.rangeDetect.range);

            if (foundEnemy.Count > 0)
            {
                _object.ReplaceAttackable(foundEnemy[0]);
            }
            else if (_object.hasAttackable)
                _object.RemoveAttackable();
        }
    }



    private List<GameEntity> CheckEnemy(Vector2 center, float radius)
    {
        var foundObject = RaycastDetect(center, radius);
        var foundEntitys = GetEntityFromFoundObject(foundObject);

        var enemies = new List<GameEntity>();

        for (int i = 0; i < foundEntitys.Count; i++)
        {
            if (foundEntitys[i].asset.name.Equals(RES.Enemy))
                enemies.Add(foundEntitys[i]);
        }

        return enemies;
    }

    private Collider2D [] RaycastDetect(Vector2 center, float radius)
    {
        var detectObjects = Physics2D.OverlapCircleAll(center, radius);
        return detectObjects;
    }

    private List<GameEntity> GetEntityFromFoundObject(Collider2D[] colliders)
    {
        var _objects = gameContext.GetGroup(GameMatcher.View).GetEntities();

        List<GameEntity> foundEntitys = new List<GameEntity>();

        for (int i = 0; i < colliders.Length; i++)
        {
            foreach (var _object in _objects)
            {
                if (_object.view.gameObject == colliders[i].gameObject)
                {
                    foundEntitys.Add(_object);
                }
            }
        }

        

        return foundEntitys;
    }

    //private GameEntity GetEntityFromFoundObject(Collider2D gameObject)
    //{
    //    var _objects = gameContext.GetGroup(GameMatcher.Asset).GetEntities();
    //    foreach (var _object in _objects)
    //    {
    //        if (_object.view.gameObject == gameObject)
    //        {
    //            return _object;
    //        }
    //    }

    //    return null;
    //}
}
