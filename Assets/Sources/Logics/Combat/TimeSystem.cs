﻿using Entitas;
using System;
using UnityEngine;

public class TimeAttackSystem : IExecuteSystem
{
    private GameContext gameContext;

    public TimeAttackSystem(Contexts context)
    {
        gameContext = context.game;
    }

    public void Execute()
    {
        var entities = gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.Attackable, GameMatcher.AttackTimer)).GetEntities();


        foreach (var entity in entities)
        {
            if(!entity.attackTimer.isPause && entity.attackTimer.value <= entity.attackTimer.targetTime)
                entity.attackTimer.value += Time.deltaTime;
        }
    }
}
