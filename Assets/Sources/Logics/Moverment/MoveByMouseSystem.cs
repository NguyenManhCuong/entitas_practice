﻿using System.Collections.Generic;
using UnityEngine;
using Entitas;
using System;

public class MoveByMouseSystem : ReactiveSystem<InputEntity> {

    private GameContext _gamecontext;
    private InputContext _inputcontext;

    public MoveByMouseSystem(Contexts contexts) : base(contexts.input)
    {
        _gamecontext = contexts.game;
        _inputcontext = contexts.input;
    }

    protected override void Execute(List<InputEntity> entities)
    {
        var player = _gamecontext.GetGroup(GameMatcher.AllOf(GameMatcher.AnimatePosition, GameMatcher.Moveable)).GetEntities();
        var heroSelected = _gamecontext.heroSelected.hero;
        
        foreach (var entity in player)
        {
            if(entity == heroSelected)
                entity.animatePosition.value = new Vector2(_inputcontext.mouse.x, _inputcontext.mouse.y);
        }
    }

    

    protected override bool Filter(InputEntity entity)
    {
        return entity.hasMouse;
    }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
    {
        return context.CreateCollector(InputMatcher.Mouse);
    }



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
