﻿using Entitas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FlySystem : IExecuteSystem {

    private GameContext gameContext;

    public FlySystem(Contexts context)
    {
        gameContext = context.game;
    }

    public void Execute()
    {
        var flyEntites = gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.Fly, GameMatcher.Position)).GetEntities();
        foreach (var entity in flyEntites)
        {
            Fly(entity);
        }
    }

    private void Fly(GameEntity flyObject)
    {
        if (!flyObject.hasFly)
            return;

        var direction = flyObject.fly.direction;
        var speed = flyObject.fly.speed;

        var newPos = flyObject.position.value + direction * speed * Time.deltaTime;
        flyObject.ReplacePosition(newPos);
    }
}
