﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using System;

public class RenderAnimatePosition : IExecuteSystem
{
    private GameContext _context;

    public RenderAnimatePosition(Contexts context)
    {
        _context = context.game;
    }

    public void Execute()
    {
        var entities = _context.GetGroup(GameMatcher.AnimatePosition).GetEntities();

        foreach (var entity in entities)
        {
            float step = entity.animatePosition.speed * Time.deltaTime;
            var realPos = entity.position.value;
            var animatePos = entity.animatePosition.value;

            if (realPos != animatePos)
            {
                Vector2 newPos = Vector2.MoveTowards(realPos, animatePos, step);
                entity.ReplacePosition(newPos);
                entity.ReplaceMoveable(true, true);
            }
            else
                entity.ReplaceMoveable(false, true);
        }
    }
}
