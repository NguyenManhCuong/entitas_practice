﻿using UnityEngine;
using Entitas;
using UnityEngine.EventSystems;

public class InputTouchSystem : IExecuteSystem{

    private InputContext inputContext;

    public InputTouchSystem(Contexts context)
    {
        inputContext = context.input;
    }

    public void Execute()
    {
        if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            var touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            inputContext.ReplaceMouse(touchPos.x, touchPos.y);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
