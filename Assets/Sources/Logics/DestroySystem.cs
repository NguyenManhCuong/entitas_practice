﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using System;

public class DestroySystem : ReactiveSystem<GameEntity> {

    public GameContext gameContext;

    public DestroySystem(Contexts context) : base(context.game)
    {
        gameContext = context.game;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (entity.destroyed.isDestoyed)
            {
                entity.Destroy();
            }
                
        }
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Destroyed);
    }


}
