﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using System;
/// <summary>
/// reactive when new hero add to game, the ui in game will change.
/// </summary>
public class DetectHeroJoinGameSystem : ReactiveSystem<GameEntity> {

    private GameContext gameContext;
    private UIContext uiContext;

    public DetectHeroJoinGameSystem(Contexts context) : base(context.game)
    {
        gameContext = context.game;
        uiContext = context.uI;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        Debug.Log("added");
        var listeners = uiContext.GetGroup(UIMatcher.HeroJoinGameListener).GetEntities();
        foreach (var entity in listeners)
        {
            entity.heroJoinGameListener.listener.ExecuteAddHero();
        }
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.HeroAdded);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
