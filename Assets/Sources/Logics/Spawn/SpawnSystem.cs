﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using System;

public class SpawnSystem : IInitializeSystem {

    private GameContext gameContext;

    public SpawnSystem(Contexts context)
    {
        gameContext = context.game;
    }

    public void Initialize()
    {
        gameContext.CreateEntity().AddHeroAdded(new List<int>());

        var hero1 = AddNewHero(RES.Hero1, Vector2.zero);
        var hero2 = AddNewHero(RES.Hero2, new Vector2(-3, -4));
        var hero3 = AddNewHero(RES.Hero3, new Vector2(-4, 0));
        var boss = gameContext.CreateEnemy(new Vector2(4, 0));

        gameContext.ReplaceHeroSelected(hero3);
    }

    public GameEntity AddNewHero(string name, Vector2 pos)
    {
        var hero = gameContext.CreateHero(pos, name);

        var listHero = gameContext.heroAdded.list; 
        listHero.Add(hero.id.value);

        gameContext.ReplaceHeroAdded(listHero);

        return hero;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
