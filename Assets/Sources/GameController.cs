﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

public class GameController : MonoBehaviour {

    private Systems _systems;
    private Contexts contexts;

    public SettingGlobal setting;

    // Use this for initialization
    void Start () {
        contexts = Contexts.sharedInstance;

        contexts.game.OnEntityCreated += AddId;
        contexts.input.OnEntityCreated += AddId;
        contexts.game.ReplaceSettingGlobal(setting);

        _systems = createSystems(contexts);
        _systems.Initialize();
    }

    void Update()
    {
        _systems.Execute();
        _systems.Cleanup();
    }

    void OnDestroy()
    {
        _systems.TearDown();
    }

    Systems createSystems(Contexts contexts)
    {
        return new Feature("Systems")

            // UI
            .Add(new DetectHeroJoinGameSystem(contexts))

            // Input
            .Add(new InputTouchSystem(contexts))
            .Add(new MoveByMouseSystem(contexts))

            // View
            .Add(new AddViewSystem(contexts))
            .Add(new RenderPosition(contexts))
            .Add(new RenderAnimatePosition(contexts))

            // move
            .Add(new FlySystem(contexts))

            //Combat
            .Add(new EnemyCheckSystem(contexts))
            .Add(new AttackSystem(contexts))
            .Add(new TimeAttackSystem(contexts))

            // Spawn
            .Add(new SpawnSystem(contexts))

            // Destroy
            .Add(new RemoveViewSystem(contexts))
            .Add(new DestroySystem(contexts));


    }

    private void AddId(IContext context, IEntity entity)
    {
        GameEntity gameEntity = entity as GameEntity;
        if (gameEntity != null) gameEntity.AddId(gameEntity.creationIndex);
        InputEntity inputEntity = entity as InputEntity;
        if (inputEntity != null) inputEntity.AddId(inputEntity.creationIndex);
    }
    private void OnDrawGizmos()
    {
        if (contexts == null)
            return;
        var rangeCheckEntity = contexts.game.GetGroup(GameMatcher.RangeDetect).GetEntities();

        if(rangeCheckEntity.Length > 0)

        foreach (var entity in rangeCheckEntity)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(entity.position.value, entity.rangeDetect.range);
        }
    }
}
