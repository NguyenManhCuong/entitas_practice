﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using Entitas.CodeGeneration.Attributes;

[Input]
[Unique]
public class MouseComponent : IComponent {
    public float x;
    public float y;
}
