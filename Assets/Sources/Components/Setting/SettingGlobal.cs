﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique, CreateAssetMenu]
public class SettingGlobal : ScriptableObject {
    public float heroSpeed;
    public float weaponFlySpeed;
}
