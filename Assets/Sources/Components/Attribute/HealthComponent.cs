﻿using UnityEngine;
using Entitas;

[Attribute]
public class HealthComponent : IComponent {
    public int value;
}
