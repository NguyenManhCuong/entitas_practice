﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

[Game]
public class AttackTimer : IComponent {
    public float value;
    public float targetTime;
    public bool isPause;
}
