﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

[Game]
public class MoveableComponent : IComponent{
    public bool isMoving;
    public bool isMoveable;
}
