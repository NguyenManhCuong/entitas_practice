﻿using Entitas;
using UnityEngine;

[Game]
public class FlyComponent : IComponent {

    public Vector2 direction;
    public float speed;
}
