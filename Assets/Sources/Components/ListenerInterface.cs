﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHeroJoinGameListener
{
    void ExecuteAddHero();
}

[Game, Unique]
public class HeroAddedComponent : IComponent
{
    public List<int> list = new List<int>();
}

[UI]
public class HeroJoinGameListenerComponent : IComponent
{
    public IHeroJoinGameListener listener;
}
