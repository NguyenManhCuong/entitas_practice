﻿using Entitas;
using UnityEngine;

[Game]
public class AnimatePositionComponent : IComponent {
    public Vector2 value;
    public float speed;
}
