﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AttributeSupport{

	public static int GetHealth(this GameEntity entity)
    {
        if (entity.hasAttribute)
            return entity.attribute.value.health.value;
        else
            return -1;
    }
}
