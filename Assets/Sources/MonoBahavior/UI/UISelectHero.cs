﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISelectHero : MonoBehaviour, IHeroJoinGameListener {

    public List<GameObject> slots;

	// Use this for initialization
	void Awake () {
        Contexts.sharedInstance.uI.CreateEntity().AddHeroJoinGameListener(this);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SelectHero(int ID)
    {
        var gameContext = Contexts.sharedInstance.game;

        gameContext.ReplaceHeroSelected(gameContext.GetEntityWithId(ID));
    }

    public void ExecuteAddHero()
    {
        var gameContext = Contexts.sharedInstance.game;
        var listHero = gameContext.heroAdded.list;

        Debug.Log(listHero.Count);

        for (int i = 0; i < listHero.Count; i++)
        {
            int ID = listHero[i];
            Debug.Log(ID);
            var button = slots[i].transform.GetChild(0).GetComponent<Button>();
            button.onClick.AddListener(() => { SelectHero(ID); });
        }
    }
}
