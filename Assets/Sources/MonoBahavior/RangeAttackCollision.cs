﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeAttackCollision : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var collisionObject = collision.gameObject.GetGameEntityFromObject();

        var attribute = collisionObject.attribute.value;

        if (attribute != null)
            attribute.ReplaceHealth(attribute.health.value - 10);

        gameObject.GetGameEntityFromObject().AddDestroyed(true);
    }
}
    